import requests
from django.shortcuts import render

# Create your views here.
from .models import Post


def main_page(request):

    print(request.FILES, request.POST)

    if request.POST.get('text'):

        post = Post.objects.create(
            text=request.POST.get('text'),
            user=request.user,
            image=request.FILES.get('image')
        )

        BOT_TOKEN = '5506387540:AAGUSoGpZyQy47KPkVvZX-E_1q7uH0svROc'

        req = requests.post(f'https://api.telegram.org/bot{BOT_TOKEN}/sendMessage', {
            'chat_id': -628356920,
            'text': f'\U0001F609 СабаТвиттер появился новый пост - "{post.text}"',
            'parse_mode': 'html'
        })

        print(req.json())

    posts = Post.objects.filter()

    return render(request, 'main.html', locals())
