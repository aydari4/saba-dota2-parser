from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Post(models.Model):
    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    text = models.CharField('Текст', max_length=180, blank=False, null=False)

    image = models.ImageField('Изображение', upload_to='images/', blank=True, null=True)

    user = models.ForeignKey(User, verbose_name='Пользователь', blank=True, null=True, on_delete=models.CASCADE)

    created_at = models.DateTimeField('Время создания', auto_now=True)

    def __str__(self):
        return f'{self.text}'