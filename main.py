import csv

import d2api
from d2api.src import entities

api = d2api.APIWrapper('71FE0085B46916106CF095417EED5B25')

match_history = api.get_match_history()

match_id = 6699159272

details = api.get_match_details(6699159272)

print(details)

with open('stata.csv', 'w') as csvfile:
    fieldnames = ['hero', 'kills', 'deaths', 'assists']
    writer = csv.writer(csvfile)

    for player in details['players']:

        writer.writerow([f"{player['hero']['hero_name']}", f"{player['kills']}", f"{player['deaths']}", f"{player['assists']}"])

# get frequency of heroes played in the latest 100 games
# heroes = {}
#
# for match in match_history['matches']:
#     for player in match['players']:
#         hero_id = player['hero']['hero_id']
#         if not hero_id in heroes:
#             heroes[hero_id] = 0
#         heroes[hero_id] += 1
#
# # print hero frequency by name
# for hero_id, freq in heroes.items():
#     print(entities.Hero(hero_id)['hero_name'], freq)