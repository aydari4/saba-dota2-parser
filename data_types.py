print('*'*5, 'Строки')

name = 'Богатые Сабы'

print(name[:6])
print(name.upper())
print(name.lower())

print(f'Мой город - {name}')
print('Старый подход = ', 'Мой город - ' + name)

print()

print('*'*5, 'Число')

age = 33
print(f'Мой возраст - {age}')
print(age + 20)

print()

iamfloat = 44.55

print('*'*5, 'Массивы')

mass = [name, age, 78, 78]

print(mass)


for item in mass:
    print('===', item)

print()

yyu = set(mass)

print('*'*5, 'Словари')

player = {
    'name': 'Faruh',
    'age': 35,
    'coutry': 'RU',
    'items': ['llo', 'yui', 'tt']
}
print(player)

print()

print('*'*5, 'Генераторы')

generator = [x for x in range(1,10)]

print(generator)
